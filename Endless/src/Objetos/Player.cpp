#include "Player.h"
namespace ENDLESSJB
{
	Player::Player()
	{
		alreadyHit = false;
		points = STARTDEFAULTNUMBER;
		myBody.height = STARTDEFAULTNUMBER;
		myBody.width = STARTDEFAULTNUMBER;
		myBody.y = STARTDEFAULTNUMBER;
		myBody.x = STARTDEFAULTNUMBER;
		myColor = WHITE;
		lives = STARTDEFAULTNUMBER;
		road = STARTDEFAULTNUMBER;
		jumpSpeed = GetScreenWidth() / 5;
		actualGroundPos = STARTDEFAULTNUMBER;
		actualJumpHeighPos = STARTDEFAULTNUMBER;
		isJumping = false;
		reachMaxHeigh = false;
		actualJumpHeighPos = GetScreenHeight() - (GetScreenHeight() / 10) - (myBody.height * DOUBLE);
		coins = STARTDEFAULTNUMBER;
		actualGroundPos = myBody.y;
		gKeys = STARTDEFAULTNUMBER;
		sKeys = STARTDEFAULTNUMBER;
		attackArea.height = STARTDEFAULTNUMBER;
		attackArea.width = STARTDEFAULTNUMBER;
		attackArea.y = STARTDEFAULTNUMBER;
		attackArea.x = STARTDEFAULTNUMBER;
		death = false;
	}

	Player::~Player()
	{
	}

	void Player::init()
	{
		alreadyHit = false;
		points = STARTDEFAULTNUMBER;
		myBody.height = Global::PLAYERHEIGHT;
		myBody.width = Global::PLAYERWIDTH;
		myBody.y = GetScreenHeight() - myBody.height - (GetScreenHeight() / 10);
		myBody.x = Global::PLAYERWIDTH * TRIPLE	;
		attackArea.height = Global::PLAYERHEIGHT;
		attackArea.width = Global::PLAYERWIDTH;
		attackArea.y = myBody.y;
		attackArea.x = myBody.x + Global::PLAYERWIDTH;
		myColor = WHITE;
		lives = STARTDEFAULTNUMBER;
		road = 1;
		isJumping = false;
		reachMaxHeigh = false;
		actualJumpHeighPos = GetScreenHeight() - (GetScreenHeight() / 10) - (myBody.height * DOUBLE);
		actualGroundPos = myBody.y;
		coins = STARTDEFAULTNUMBER;
		gKeys = STARTDEFAULTNUMBER;
		sKeys = STARTDEFAULTNUMBER;
		death = false;
	}

	void Player::input()
	{
		
		if (IsKeyReleased(KEY_W) )
		{
			if (isJumping == false)
			{
				roadUp();
				switch (getRoad())
				{
				case 0:
					myBody.y = GetScreenHeight() - myBody.height;
					actualGroundPos = myBody.y;
					actualJumpHeighPos = GetScreenHeight() - (myBody.height * DOUBLE);
					attackArea.y = myBody.y;
					break;
				case 1:
					myBody.y = GetScreenHeight() - myBody.height - (GetScreenHeight() / 10);
					actualGroundPos = myBody.y;
					actualJumpHeighPos = GetScreenHeight() - (myBody.height * DOUBLE) - (GetScreenHeight() / 10);
					attackArea.y = myBody.y;
					break;
				case 2:
					myBody.y = GetScreenHeight() - myBody.height - (GetScreenHeight() / 5);
					actualGroundPos = myBody.y;
					actualJumpHeighPos = GetScreenHeight() - (myBody.height * DOUBLE) - (GetScreenHeight() / 5);
					attackArea.y = myBody.y;
					break;
				default:
					break;
				}
			}
		}
		if (IsKeyReleased(KEY_S) )
		{
			if (isJumping == false)
			{
				roadDown();
				switch (getRoad())
				{
				case 0:
					myBody.y = GetScreenHeight() - myBody.height;
					actualJumpHeighPos = GetScreenHeight() - (myBody.height *DOUBLE);
					actualGroundPos = myBody.y;
					attackArea.y = myBody.y;
					break;
				case 1:
					myBody.y = GetScreenHeight() - myBody.height - (GetScreenHeight() / 10);
					actualJumpHeighPos = GetScreenHeight() - (GetScreenHeight() / 10) - (myBody.height * DOUBLE);
					actualGroundPos = myBody.y;
					attackArea.y = myBody.y;
					break;
				case 2:
					myBody.y = GetScreenHeight() - myBody.height - (GetScreenHeight() / 5);
					actualJumpHeighPos = GetScreenHeight() - (GetScreenHeight() / 5) - (myBody.height * DOUBLE);
					actualGroundPos = myBody.y;
					attackArea.y = myBody.y;
					break;
				default:
					break;
				}
			}
		}
	}

	void Player::update()
	{

		if (isJumping == true)
		{
			if (reachMaxHeigh == true)
			{
				myBody.y = myBody.y + (jumpSpeed * GetFrameTime());
				attackArea.y = myBody.y;
				if (myBody.y >= actualGroundPos)
				{
					isJumping = false;
					reachMaxHeigh = false;
				}
			}
			else
			{
				myBody.y = myBody.y - (jumpSpeed * GetFrameTime());
				attackArea.y = myBody.y;
				if (myBody.y <= actualJumpHeighPos)
				{
					reachMaxHeigh = true;
				}
			}
		}

		


	}

	void Player::draw()
	{
		DrawText(TextFormat(" %1i", coins), GetScreenWidth() - (GetScreenWidth() / 5) - MeasureText(":", Global::TextSize / HALFDIVIDER) / HALFDIVIDER, GetScreenHeight() / 8 - Global::TextSize, Global::TextSize / HALFDIVIDER, YELLOW);

		DrawText(TextFormat(" %1i", sKeys), GetScreenWidth() - (GetScreenWidth() / 6) - MeasureText(":", Global::TextSize / HALFDIVIDER) / HALFDIVIDER, GetScreenHeight() / 3.4 - Global::TextSize, Global::TextSize / HALFDIVIDER, WHITE);
		DrawText(TextFormat(" %1i", gKeys), GetScreenWidth() - (GetScreenWidth() / 6) - MeasureText(":", Global::TextSize / HALFDIVIDER) / HALFDIVIDER, GetScreenHeight() / 4.6 - Global::TextSize, Global::TextSize / HALFDIVIDER, WHITE);

	}

	Rectangle Player::getBody()
	{
		return myBody;
	}

	Vector2 Player::getPosition()
	{
		Vector2 pos = { getBody().x - (getBody().width *3), getBody().y - (getBody().height/1.5)};
		return pos;
	}

	int Player::getLives()
	{
		return lives;
	}

	int Player::getPoints()
	{
		return points;
	}

	bool Player::getAlreadyHit()
	{
		return alreadyHit;
	}

	int Player::getRoad()
	{
		return road;
	}

	int Player::getCoins()
	{
		return coins;
	}

	int Player::getSKeys()
	{
		return sKeys;
	}

	int Player::getGKeys()
	{
		return gKeys;
	}

	bool Player::getIsJumping()
	{
		return isJumping;
	}

	bool Player::getReachMaxHeigh()
	{
		return reachMaxHeigh;
	}

	Rectangle Player::getAttackArea()
	{
		return attackArea;
	}

	bool Player::getDeath()
	{
		return death;
	}

	void Player::setAlreadyHit(bool dohit)
	{
		alreadyHit = dohit;
	}

	void Player::setPoints(int npoints)
	{
		points = npoints;
	}

	void Player::setLives(int nLives)
	{
		lives = nLives;
	}

	void Player::setCoins(int nCoins)
	{
		coins = nCoins;
	}

	void Player::setSKeys(int nSKeys)
	{
		sKeys = nSKeys;
	}

	void Player::setGKeys(int nGKeys)
	{
		gKeys = nGKeys;
	}

	void Player::setIsJumping(bool jumping)
	{
		isJumping = jumping;
	}

	void Player::setReachMaxHeigh(bool reach)
	{
		reachMaxHeigh = reach;
	}

	void Player::setDeath(bool deathStatus)
	{
		death = deathStatus;
	}

	void Player::roadUp()
	{
		if (road < 2) road++;
	}

	void Player::roadDown()
	{
		if (road > 0) road--;
	}

}

#ifndef PLAYER_H
#define PLAYER_H
#include "raylib.h"
#include "Escenas/Global.h"
namespace ENDLESSJB
{
	class Player
	{
	public:
		Player();
		~Player();
		void init();
		void input();
		void update();
		void draw();

		Rectangle getBody();
		Vector2 getPosition();
		int getLives();
		int getPoints();
		bool getAlreadyHit();
		int getRoad();
		int getCoins();
		int getSKeys();
		int getGKeys();
		bool getIsJumping();
		bool getReachMaxHeigh();
		Rectangle getAttackArea();
		bool getDeath();

		void setAlreadyHit(bool dohit);
		void setPoints(int npoints);
		void setLives(int nLives);
		void setCoins(int nCoins);
		void setSKeys(int nSKeys);
		void setGKeys(int nGKeys);
		void setIsJumping(bool jumping);
		void setReachMaxHeigh(bool reach);
		void setDeath(bool deathStatus);
		void roadUp();
		void roadDown();

	private:
		bool isJumping;
		bool reachMaxHeigh;
		bool death;
		bool alreadyHit;
		int points;
		int lives;
		int coins;
		int gKeys;
		int sKeys;
		Rectangle myBody;
		Rectangle attackArea;
		Color myColor;
		int road;
		float jumpSpeed;
		float actualGroundPos;
		float actualJumpHeighPos;
		//Texture2D myTexture;
	};
}
#endif

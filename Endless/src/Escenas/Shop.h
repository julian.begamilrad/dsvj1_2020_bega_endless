#ifndef SHOP_H
#define SHOP_H

#include "raylib.h"

#include "Global.h"
#include "Textures.h"

namespace ENDLESSJB
{
	class Shop
	{
	public:

		Shop();
		~Shop();
		void Init();
		void Input();
		void Update();
		void sDraw();
		Textures shopTextures;
		Texture2D shopBase;

		int currentFrame;
		int framesCounter;
		int framesSpeed;

		int charactersPrice[CHARACTERSAVALIEBLE];
		int backgroundsPrice[BACKGROUNDSAVALIEBLE];
		int actualPrice;
	}
	;
}
#endif
#ifndef MENU_H
#define MENU_H

#include "raylib.h"

#include "Global.h"
#include "Textures.h"

namespace ENDLESSJB
{
	class Menu
	{
		enum MenuOptions
		{
			Play, Config, Credits, Shop, Exit
		};

	public:
		Menu();
		~Menu();
		void mInit();
		void mInput();
		void mUpdate();
		void mDraw();

		Textures menuTextures;
	private:

		int actualOption;
		bool isControlMenu;

		Sound select;
		Sound moveSelection;
		Music music;
		bool musicOn;
		Texture2D menuBase;
		Texture2D controls;

		int currentFrame ;
		int framesCounter;
		int framesSpeed;
	};

}
#endif
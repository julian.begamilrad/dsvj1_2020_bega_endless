#ifndef GAMESTRUCTURE_H
#define GAMESTRUCTURE_H

#include "Global.h"
#include "Game.h"
#include "Menu.h"
#include "Shop.h"
#include "HighScores.h"
#include "Credits.h"

namespace ENDLESSJB
{
	//1024�576
	//1280 x 720
	//1366�768
	//1600�900
	//1920 x 1080
	const int screenWidth = 1024;
	const int screenHeight = 576;
	class GameStructure
	{
	public:
		GameStructure();
		~GameStructure();
		void gameLoop();
		void gsInit();
		void gsInput();
		void gsUpdate();
		void gsDraw();
		void gsDeInit();		

		Game* game;
		Menu* menu;
		Credits* credits;
		Shop* shop;
		HighScores* highscores;
		bool firstTime;

	private:
		bool inGame;

	};

}
#endif
#include "SpawnPaterns.h"
namespace ENDLESSJB
{
	SpawnPaterns::SpawnPaterns()
	{
		for (int j = 0; j < 3; j++)
		{
			for (int i = 0; i < 25; i++)
			{
				Pattern[j][i].Body.height = Global::PLAYERWIDTH;
				Pattern[j][i].Body.width = Global::PLAYERWIDTH;
				Pattern[j][i].Body.y = GetScreenHeight() - ((GetScreenHeight() / 10) * (j + 1)) + (Global::PLAYERWIDTH / THIRDDIVIDER);
				Pattern[j][i].Body.x = (GetScreenWidth()) + Global::PLAYERWIDTH * (i + 1);
				//Pattern[j][i].Body.x = 0 + Global::PLAYERWIDTH * (i + 1);
				Pattern[j][i].position.x = Pattern[j][i].Body.x;
				Pattern[j][i].position.y = Pattern[j][i].Body.y;
				Pattern[j][i].DownPos.x = Pattern[j][i].Body.x;
				Pattern[j][i].DownPos.y = Pattern[j][i].Body.y;
				Pattern[j][i].UpPos.x = Pattern[j][i].Body.x;
				Pattern[j][i].UpPos.y = Pattern[j][i].Body.y - (Global::PLAYERWIDTH);
				Pattern[j][i].type = COIN;
				Pattern[j][i].speed = SPEED;
				Pattern[j][i].goUp = true;
			}
		}
		PatternNumber = 0;
		allreadySetPattern = false;
		coin = LoadTexture("res/Textures/Coin.png");
		barrel = LoadTexture("res/Textures/barrel.png");
		brokenBarrel = LoadTexture("res/Textures/BrokenBarrel.png");
		diamond = LoadTexture("res/Textures/Diamond.png");
		poison = LoadTexture("res/Textures/PoisonCloud.png");
		spikes = LoadTexture("res/Textures/Spikes.png");
		gchest = LoadTexture("res/Textures/Gchest.png");
		schest = LoadTexture("res/Textures/Schest.png");
		gchestOpen = LoadTexture("res/Textures/GchestOpen.png");
		schestOpen = LoadTexture("res/Textures/SchestOpen.png");
		gkey = LoadTexture("res/Textures/Gkey.png");
		skey = LoadTexture("res/Textures/Skey.png");
	}
	SpawnPaterns::~SpawnPaterns()
	{
		UnloadTexture(coin);
		UnloadTexture(barrel);
		UnloadTexture(brokenBarrel);
		UnloadTexture(spikes);
		UnloadTexture(poison);
		UnloadTexture(gchest);
		UnloadTexture(schest);
		UnloadTexture(gchestOpen);
		UnloadTexture(schestOpen);
		UnloadTexture(gkey);
		UnloadTexture(skey);
		UnloadTexture(diamond);
	}

	void SpawnPaterns::patternInit(int myNumber)
	{
		for (int j = 0; j < 3; j++)
		{
			for (int i = 0; i < 25; i++)
			{
				Pattern[j][i].Body.height = Global::PLAYERWIDTH;
				Pattern[j][i].Body.width = Global::PLAYERWIDTH;
				Pattern[j][i].Body.y = (GetScreenHeight() - ((GetScreenHeight() / 10) * (j+ 1)) + (Global::PLAYERWIDTH / THIRDDIVIDER));
				Pattern[j][i].Body.x = (GetScreenWidth() * myNumber) + Global::PLAYERWIDTH * (i + 1);
				//Pattern[j][i].Body.x = 0 + Global::PLAYERWIDTH * (i + 1);
				Pattern[j][i].position.x = Pattern[j][i].Body.x;
				Pattern[j][i].position.y = Pattern[j][i].Body.y;
				Pattern[j][i].type = COIN;
			}
		}
		allreadySetPattern = false;
		setNewPattern();

	}

	void SpawnPaterns::patternDraw()
	{
		float baseScale = GetScreenHeight();
		float scale = baseScale / 1080;

		for (int j = 0; j < 3; j++)
		{
			for (int i = 0; i < 25; i++)
			{
				switch (Pattern[j][i].type)
				{
				case NONE:
					break;
				case COIN:
					DrawTextureEx(coin, Pattern[j][i].position, 0, scale / 2.4, WHITE);
					break;
				case SPIKES:
					DrawTextureEx(spikes, Pattern[j][i].position, 0, scale / THIRDDIVIDER, WHITE);
					break;
				case SKEY:
					DrawTextureEx(skey, Pattern[j][i].position, 0, scale / 5, WHITE);
					break;
				case GKEY:
					DrawTextureEx(gkey, Pattern[j][i].position, 0, scale / 5, WHITE);
					break;
				case SCHEST:
					DrawTextureEx(schest, Pattern[j][i].position, 0, scale / THIRDDIVIDER, WHITE);
					break;
				case GCHEST:
					DrawTextureEx(gchest, Pattern[j][i].position, 0, scale / THIRDDIVIDER, WHITE);
					break;
				case BARREL:
					DrawTextureEx(barrel, Pattern[j][i].position, 0, scale/1.55, WHITE);
					break;
				case DIAMOND:
					DrawTextureEx(diamond, Pattern[j][i].position, 0, scale / THIRDDIVIDER, WHITE);
					break;
				case SCHESTOPEN:
					DrawTextureEx(schestOpen, Pattern[j][i].position, 0, scale / THIRDDIVIDER, WHITE);
					break;
				case GCHESTOPEN:
					DrawTextureEx(gchestOpen, Pattern[j][i].position, 0, scale / THIRDDIVIDER, WHITE);
					break;
				case POISON:
					Vector2 poisonPos = Pattern[j][i].position;
					poisonPos.y = poisonPos.y - Global::PLAYERWIDTH / 1.5;
					DrawTextureEx(poison, Pattern[j][i].position, 0, scale / THIRDDIVIDER, WHITE);
					DrawTextureEx(poison, poisonPos, 0, scale / THIRDDIVIDER, WHITE);
					break;
				case BROKENBARREL:
					DrawTextureEx(brokenBarrel, Pattern[j][i].position, 0, scale / 1.55, WHITE);
					break;
				default:
					break;
				}
			}
		}

	}

	void SpawnPaterns::patternParallax()
	{
		
		for (int j = 0; j < 3; j++)
		{
			for (int i = 0; i < 25; i++)
			{
				Pattern[j][i].position.x = Pattern[j][i].position.x - ((Global::parallaxMovment + (Global::parallaxMovment * 5)) * GetFrameTime());
				Pattern[j][i].Body.x = Pattern[j][i].position.x;
				if (Pattern[0][0].position.x <= -(GetScreenWidth()))
				{
					setNewPattern();
				}
				if (Pattern[j][i].position.x <= -(GetScreenWidth()))
				{
					Pattern[j][i].position.x = GetScreenWidth();
				}
				if (Pattern[j][i].type == COIN || Pattern[j][i].type == DIAMOND || Pattern[j][i].type == POISON)
				{				
					if (Pattern[j][i].position.y <= Pattern[j][i].UpPos.y)
					{
						Pattern[j][i].goUp = false;
					}
					else if (Pattern[j][i].position.y >= Pattern[j][i].DownPos.y)
					{
						Pattern[j][i].goUp = true;
					}	
					if (Pattern[j][i].goUp == true)
					{
						Pattern[j][i].position.y = Pattern[j][i].position.y - (Pattern[j][i].speed * GetFrameTime());
					}
					else
					{
						Pattern[j][i].position.y = Pattern[j][i].position.y + (Pattern[j][i].speed * GetFrameTime());
					}
				}
			}
		}
		
	}

	void SpawnPaterns::setNewPattern()
	{
		int actualPattern = GetRandomValue(0, 16);
		switch (actualPattern)
		{
		case 0:

			for (int j = 0; j < 3; j++)
			{
				for (int i = 0; i < 25; i++)
				{
					Pattern[j][i].type = NONE;
				}
			}
			Pattern[0][5].type = COIN;
			Pattern[0][8].type = COIN;
			Pattern[0][11].type = COIN;
			Pattern[0][14].type = COIN;
			Pattern[0][17].type = COIN;
			break;
		case 1:
			for (int j = 0; j < 3; j++)
			{
				for (int i = 0; i < 25; i++)
				{
					Pattern[j][i].type = NONE;
				}
			}
			Pattern[1][5].type = COIN;
			Pattern[1][8].type = COIN;
			Pattern[1][11].type = COIN;
			Pattern[1][14].type = COIN;
			Pattern[1][17].type = COIN;
			break;
		case 2:
			for (int j = 0; j < 3; j++)
			{
				for (int i = 0; i < 25; i++)
				{
					Pattern[j][i].type = NONE;
				}
			}
			Pattern[2][5].type = COIN;
			Pattern[2][8].type = COIN;
			Pattern[2][11].type = COIN;
			Pattern[2][14].type = COIN;
			Pattern[2][17].type = COIN;
			break;
		case 3:
			for (int j = 0; j < 3; j++)
			{
				for (int i = 0; i < 25; i++)
				{
					Pattern[j][i].type = NONE;
				}
			}
			Pattern[0][5].type = COIN;
			Pattern[0][8].type = COIN;
			Pattern[0][11].type = COIN;
			Pattern[0][14].type = COIN;
			Pattern[0][17].type = COIN;
			Pattern[2][11].type = SKEY;
			break;
		case 4:
			for (int j = 0; j < 3; j++)
			{
				for (int i = 0; i < 25; i++)
				{
					Pattern[j][i].type = NONE;
				}
			}
			Pattern[0][5].type = COIN;
			Pattern[0][8].type = COIN;
			Pattern[0][11].type = COIN;
			Pattern[0][14].type = COIN;
			Pattern[0][17].type = COIN;
			Pattern[2][11].type = GKEY;
			break;
		case 5:
			for (int j = 0; j < 3; j++)
			{
				for (int i = 0; i < 25; i++)
				{
					Pattern[j][i].type = NONE;
				}
			}
			Pattern[2][5].type = COIN;
			Pattern[2][8].type = COIN;
			Pattern[2][11].type = COIN;
			Pattern[2][14].type = COIN;
			Pattern[2][17].type = COIN;
			Pattern[0][11].type = GKEY;
			break;
		case 6:
			for (int j = 0; j < 3; j++)
			{
				for (int i = 0; i < 25; i++)
				{
					Pattern[j][i].type = NONE;
				}
			}
			Pattern[2][5].type = COIN;
			Pattern[2][8].type = COIN;
			Pattern[2][11].type = COIN;
			Pattern[2][14].type = COIN;
			Pattern[2][17].type = COIN;
			Pattern[0][11].type = SKEY;
			break;
		case 7:
			for (int j = 0; j < 3; j++)
			{
				for (int i = 0; i < 25; i++)
				{
					Pattern[j][i].type = NONE;
				}
			}
			Pattern[2][5].type = COIN;
			Pattern[2][8].type = COIN;
			Pattern[2][11].type = COIN;
			Pattern[2][14].type = COIN;
			Pattern[2][17].type = COIN;
			Pattern[0][11].type = GCHEST;
			break;
		case 8:
			for (int j = 0; j < 3; j++)
			{
				for (int i = 0; i < 25; i++)
				{
					Pattern[j][i].type = NONE;
				}
			}
			Pattern[2][5].type = COIN;
			Pattern[2][8].type = COIN;
			Pattern[2][11].type = COIN;
			Pattern[2][14].type = COIN;
			Pattern[2][17].type = COIN;
			Pattern[0][11].type = SCHEST;
			break;
		case 9:
			for (int j = 0; j < 3; j++)
			{
				for (int i = 0; i < 25; i++)
				{
					Pattern[j][i].type = NONE;
				}
			}
			Pattern[0][5].type = COIN;
			Pattern[0][8].type = COIN;
			Pattern[0][11].type = COIN;
			Pattern[0][14].type = COIN;
			Pattern[0][17].type = COIN;
			Pattern[2][11].type = SCHEST;
			break;
		case 10:
			for (int j = 0; j < 3; j++)
			{
				for (int i = 0; i < 25; i++)
				{
					Pattern[j][i].type = NONE;
				}
			}
			Pattern[0][5].type = COIN;
			Pattern[0][8].type = COIN;
			Pattern[0][11].type = COIN;
			Pattern[0][14].type = COIN;
			Pattern[0][17].type = COIN;
			Pattern[2][11].type = GCHEST;
			break;
		case 11:
			for (int j = 0; j < 3; j++)
			{
				for (int i = 0; i < 25; i++)
				{
					Pattern[j][i].type = NONE;
				}
			}
			Pattern[0][5].type = COIN;
			Pattern[0][8].type = COIN;
			Pattern[0][11].type = COIN;
			Pattern[0][14].type = COIN;
			Pattern[0][17].type = COIN;
			Pattern[1][7].type = SPIKES;
			Pattern[1][8].type = SPIKES;
			Pattern[1][14].type = SPIKES;
			Pattern[1][15].type = SPIKES;
			Pattern[2][11].type = GCHEST;
			break;
		case 12:
			for (int j = 0; j < 3; j++)
			{
				for (int i = 0; i < 25; i++)
				{
					Pattern[j][i].type = NONE;
				}
			}
			Pattern[0][5].type = COIN;
			Pattern[0][11].type = COIN;
			Pattern[0][17].type = COIN;
			Pattern[1][7].type = SPIKES;
			Pattern[1][8].type = SPIKES;
			Pattern[1][11].type = DIAMOND;
			Pattern[1][14].type = SPIKES;
			Pattern[1][15].type = SPIKES;
			Pattern[2][11].type = GCHEST;
			break;
		case 13:
			for (int j = 0; j < 3; j++)
			{
				for (int i = 0; i < 25; i++)
				{
					Pattern[j][i].type = NONE;
				}
			}
			Pattern[0][5].type = COIN;
			Pattern[0][11].type = COIN;
			Pattern[0][17].type = COIN;
			Pattern[1][8].type = SPIKES;
			Pattern[1][11].type = DIAMOND;
			Pattern[1][15].type = SPIKES;
			Pattern[2][11].type = POISON;
			break;
		case 14:
			for (int j = 0; j < 3; j++)
			{
				for (int i = 0; i < 25; i++)
				{
					Pattern[j][i].type = NONE;
				}
			}
			Pattern[0][5].type = POISON;
			Pattern[1][5].type = POISON;
			Pattern[0][17].type = COIN;
			Pattern[1][7].type = SPIKES;
			Pattern[1][10].type = DIAMOND;
			Pattern[1][15].type = SPIKES;
			Pattern[2][11].type = GCHEST;
			break;
		case 15:
			for (int j = 0; j < 3; j++)
			{
				for (int i = 0; i < 25; i++)
				{
					Pattern[j][i].type = NONE;
				}
			}
			Pattern[0][5].type = POISON;
			Pattern[1][5].type = BARREL;
			Pattern[1][5].type = SPIKES;
			Pattern[0][17].type = COIN;
			Pattern[1][8].type = SPIKES;
			Pattern[1][15].type = SPIKES;
			Pattern[2][11].type = GKEY;
			break;
		case 16:
			for (int j = 0; j < 3; j++)
			{
				for (int i = 0; i < 25; i++)
				{
					Pattern[j][i].type = NONE;
				}
			}
			Pattern[0][5].type = SPIKES;
			Pattern[1][5].type = SPIKES;
			Pattern[2][5].type = SPIKES;
			Pattern[0][10].type = POISON;
			Pattern[1][10].type = POISON;
			Pattern[2][10].type = COIN;
			Pattern[0][15].type = SPIKES;
			Pattern[1][15].type = SPIKES;
			Pattern[2][15].type = BARREL;
			break;
		default:
			break;
		}
	}
}


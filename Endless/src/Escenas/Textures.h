#ifndef TEXTURES_H
#define TEXTURES_H

#include "raylib.h"
namespace ENDLESSJB
{
	enum AnimationsPosibilities
	{
		RUN, JUMP, DIE, ATTACK, IDLE
	};
	enum Characters
	{
		GOLDENKNIGHT, BLACKKNIGHT, BRONZEKNIGHT
	};
	class Textures
	{
	public:
		Textures();
		~Textures();

		
		Texture2D BackGrounds[5][4];
		//Background Textures
		Texture2D sky[4];
		Texture2D farBackground[4];
		Texture2D ground[4];
		Texture2D middleBackground[4];
		Texture2D closeBackground[4];
		Texture2D playerSprites[7][5][10];

	private:
	};
}
#endif
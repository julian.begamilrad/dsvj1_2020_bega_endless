#include "Menu.h"
namespace ENDLESSJB
{
	Menu::Menu()
	{
		actualOption = Play;
		Global::setGamestatus(MENU);
		isControlMenu = false;
		select = LoadSound("res/Select.ogg");
		moveSelection = LoadSound("res/MoveSelection.ogg");
		music = LoadMusicStream("res/MenuMusic.mp3");
		menuBase = LoadTexture("res/Textures/Menu/MenuBase.png");
		controls = LoadTexture("res/Textures/Menu/MenuControls.png");
		musicOn = true;
		currentFrame = STARTDEFAULTNUMBER;
		framesCounter = STARTDEFAULTNUMBER;
		framesSpeed = 8;
	}
	Menu::~Menu()
	{
		UnloadSound(select);     
		UnloadSound(moveSelection);
		UnloadMusicStream(music);
		UnloadTexture(menuBase);
		UnloadTexture(controls);
	}
	void Menu::mInit()
	{
		actualOption = Play;
		Global::setGamestatus(MENU);
		isControlMenu = false;
		PlayMusicStream(music);
		musicOn = true;
		currentFrame = STARTDEFAULTNUMBER;
		framesCounter = STARTDEFAULTNUMBER;
		framesSpeed = 8;
	}
	void Menu::mInput()
	{
		if (IsKeyReleased(KEY_Q))
		{
			Global::ChangeSize();
		}
		if (IsKeyReleased(KEY_N))
		{
			if (musicOn)PauseMusicStream(music);
			else ResumeMusicStream(music);
			musicOn = !musicOn;
		}
		if (IsKeyReleased(KEY_S) || IsKeyReleased(KEY_DOWN))
		{
			if (isControlMenu == false)
			{
				PlaySound(moveSelection);

				if (actualOption == Exit)
				{
					actualOption = Play;
				}
				else
				{
					actualOption++;
				}
			}
		}

		if (IsKeyReleased(KEY_W) || IsKeyReleased(KEY_UP))
		{
			if (isControlMenu == false)
			{
				PlaySound(moveSelection);
				if (actualOption == Play)
				{
					actualOption = Exit;
				}
				else
				{
					actualOption--;
				}
			}
		}
		if (IsKeyReleased(KEY_ENTER))
		{
			PlaySound(select);
			if (isControlMenu == false)
			{
				switch (actualOption)
				{
				case Play:
					Global::setGamestatus(GAME);
					break;
				case Config:
					isControlMenu = true;
					break;
				case Credits:
					Global::setGamestatus(CREDITS);
					break;
				case Shop:
					Global::setGamestatus(SHOP);
					break;
				case Exit:
					Global::setGamestatus(EXIT);
					break;
				default:
					break;
				}

			}
			else
			{
				isControlMenu = false;
			}
		}
		if (IsKeyDown(KEY_ESCAPE))
		{
			Global::setGamestatus(EXIT);
		}
	}
	void Menu::mUpdate()
	{
		framesCounter++;
		UpdateMusicStream(music);
		if (framesCounter >= (60 / framesSpeed))
		{
			framesCounter = 0;
			currentFrame++;

			if (currentFrame > 9) currentFrame = 0;
		}
	}
	void Menu::mDraw()
	{
		float baseScale = GetScreenHeight();
		float scale = baseScale / 1080;
		Vector2 pos;
		pos.x = 0; pos.y = 0;
		Vector2 posPlayer;
		posPlayer.x = GetScreenWidth()/3; posPlayer.y = GetScreenHeight()/2.5;
		float PJScale = scale / 1.3;

		for (int i = 0; i < 5; i++)
		{
			DrawTextureEx(menuTextures.BackGrounds[i][Global::actualBackground], pos, 0, scale, WHITE);
		}

			DrawTextureEx(menuTextures.playerSprites[Global::actualCharacter][IDLE][currentFrame], posPlayer, 0, PJScale, WHITE);

		
		
		if (isControlMenu == false)
		{
			DrawTextureEx(menuBase, pos, 0, scale / 1.24, WHITE);
			
			DrawText("Play", Global::DISTANCEDEFAULT * 1.8, Global::DISTANCEDEFAULT * 2.6, Global::DISTANCEDEFAULT / 1.2, WHITE);
			DrawText("Controls", Global::DISTANCEDEFAULT * 1.8, Global::DISTANCEDEFAULT * 4.8, Global::DISTANCEDEFAULT / 1.8, WHITE);
			DrawText("Credits", Global::DISTANCEDEFAULT * 1.8, Global::DISTANCEDEFAULT * 6.8, Global::DISTANCEDEFAULT / 1.7, WHITE);
			DrawText("Shop", Global::DISTANCEDEFAULT * 1.8, Global::DISTANCEDEFAULT * 8.9, Global::DISTANCEDEFAULT / 1.2, WHITE);
			DrawText("Exit", Global::DISTANCEDEFAULT * 1.8, Global::DISTANCEDEFAULT * 10.8, Global::DISTANCEDEFAULT, WHITE);

			switch (actualOption)
			{
			case Play:
				DrawText("Play", Global::DISTANCEDEFAULT * 1.8, Global::DISTANCEDEFAULT * 2.6, Global::DISTANCEDEFAULT / 1.2, YELLOW);
				break;

			case Config:
				DrawText("Controls", Global::DISTANCEDEFAULT * 1.8, Global::DISTANCEDEFAULT * 4.8, Global::DISTANCEDEFAULT / 1.8, YELLOW);
				break;

			case Credits:
				DrawText("Credits", Global::DISTANCEDEFAULT * 1.8, Global::DISTANCEDEFAULT * 6.8, Global::DISTANCEDEFAULT / 1.7, YELLOW);
				break;

			case Shop:
				DrawText("Shop", Global::DISTANCEDEFAULT * 1.8, Global::DISTANCEDEFAULT * 8.9, Global::DISTANCEDEFAULT / 1.2, YELLOW);
				break;

			case Exit:
				DrawText("Exit", Global::DISTANCEDEFAULT * 1.8, Global::DISTANCEDEFAULT * 10.8, Global::DISTANCEDEFAULT, YELLOW);
				break;
			default:
				break;
			}
			DrawText(TextFormat(" %1i", Global::acumulatedCoins), GetScreenWidth() - (GetScreenWidth() / 7) - MeasureText(":", Global::TextSize / HALFDIVIDER) / HALFDIVIDER, GetScreenHeight() / 11.3, Global::TextSize / HALFDIVIDER, WHITE);


		}
		if (isControlMenu == true)
		{
			DrawTextureEx(controls, pos, 0, scale / 1.24, WHITE);
		}
	}
}

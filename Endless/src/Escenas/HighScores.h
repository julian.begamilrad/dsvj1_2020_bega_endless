#ifndef HIGHSCORES_H
#define HIGHSCORES_H

#include "raylib.h"

#include "Global.h"
#include "Textures.h"

namespace ENDLESSJB
{

	class HighScores
	{
	public:

		HighScores();
		~HighScores();
		void Init();
		void Input();
		void Update();
		void Draw();
		Texture2D scoreBase;
		int currentFrame;
		int framesCounter;
		int framesSpeed;
	}
	;
}
#endif
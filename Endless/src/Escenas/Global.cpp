#include "Global.h"
namespace ENDLESSJB
{
	float Global::TextSize = 0;
	float Global::DISTANCEDEFAULT = 0;
	float Global::PLAYERWIDTH = 0;
	float Global::PLAYERHEIGHT = 0;

	float Global::parallaxMovment = 0;
	int Global::gamestatus = MENU;
	int Global::lastGamestatus = MENU;
	int Global::actualCharacter = STARTDEFAULTNUMBER;
	int Global::actualBackground = STARTDEFAULTNUMBER;
	int Global::acumulatedCoins = STARTDEFAULTNUMBER;

	int Global::highScores[SHOWNSCORES] = { STARTDEFAULTNUMBER, STARTDEFAULTNUMBER, STARTDEFAULTNUMBER, STARTDEFAULTNUMBER,STARTDEFAULTNUMBER,STARTDEFAULTNUMBER,STARTDEFAULTNUMBER,STARTDEFAULTNUMBER,STARTDEFAULTNUMBER,STARTDEFAULTNUMBER };
	bool Global::unlockedCharacters[3] = {true, false, false};
	bool Global::unlockedBackgrounds[4] = { true, false, false, false };


	Global::Global()
	{
		gamestatus = MENU;
		lastGamestatus = MENU;
		TextSize = GetScreenHeight() / 12;
		DISTANCEDEFAULT = GetScreenHeight() / 15;
		parallaxMovment = GetScreenWidth() / 16;
		actualCharacter = STARTDEFAULTNUMBER;
		actualBackground = STARTDEFAULTNUMBER;
		acumulatedCoins = STARTDEFAULTNUMBER;
		for (int i = 0; i < 10; i++)
		{
			highScores[i] = STARTDEFAULTNUMBER;
		}
		unlockedCharacters[0] = true;
		for (int i = 1; i < 3; i++)
		{
			unlockedCharacters[i] = false;
		}
		unlockedBackgrounds[0] = true;
		for (int i = 1; i < 4; i++)
		{
			unlockedBackgrounds[i] = false;
		}
	}

	Global::~Global()
	{
		
		
	}
	int Global::getGamestatus()
	{
		return gamestatus;
	}
	int Global::getLastGamestatus()
	{
		return lastGamestatus;
	}
	void Global::setSize()
	{
		TextSize = GetScreenHeight() / 12;
		DISTANCEDEFAULT = GetScreenHeight() / 15;
		PLAYERWIDTH = GetScreenWidth() / 25;
		PLAYERHEIGHT = GetScreenHeight() / 8;
		parallaxMovment = GetScreenWidth() / 16;
	}
	void Global::ChangeSize()
	{
		SetWindowSize(1600, 900);
		setSize();
	}
	void Global::setGamestatus(int ngstatus)
	{
		gamestatus = ngstatus;
	}
	void Global::setLastGamestatus(int nlgstatus)
	{
		lastGamestatus = nlgstatus;
	}

	void Global::setActualCharacter(int ncharacter)
	{
		actualCharacter = ncharacter;
	}

	void Global::setActualBackground(int nbackground)
	{
		actualBackground = nbackground;
	}

	int Global::getActualCharacter()
	{
		return actualCharacter;
	}

	int Global::getActualBackground()
	{
		return actualBackground;
	}



	void Global::loadCoins()
	{
		char line[20] = "";
		ifstream FileCoinEnt;
		FileCoinEnt.exceptions(ifstream::failbit | ifstream::badbit);
		try {
			FileCoinEnt.open("Coins.txt", ios::in | ios::app);
			cout << "abriendo el archivo Coins" << endl;
			try {
				cout << "leyendo todo el archivo Coins" << endl;
				FileCoinEnt.getline(line, 20);
				while (!FileCoinEnt.eof()) {
					acumulatedCoins = stoi(line);
					cout << line << endl;
					FileCoinEnt.getline(line, 20);
				}
				cout << line << endl;
			}
			catch (ifstream::failure& e) {
				if (FileCoinEnt.bad())
					cout << "hubo un error al leer el archivo Coins";
			}
			try { FileCoinEnt.close(); cout << "cerrando el archivo Coins" << endl; }
			catch (ifstream::failure& e) { if (FileCoinEnt.fail())cout << "Hubo un error al cerrar el archivo Coins"; }
		}
		catch (ifstream::failure& e) { if (FileCoinEnt.fail())cout << "Hubo un error al abrir el archivo Coins"; }
		catch (...) {
			cout << "error desconocido" << endl;
		}
	}

	void Global::saveCoins()
	{
		char cadena[80] = "";
		ofstream fexit;
		fexit.open("Coins.txt", ios::out);
		for (int i = 0; i < 10; i++)
		{
			fexit << acumulatedCoins << endl;
		}

		fexit.close();
	}

	void Global::resetCoins()
	{
		char cadena[80] = "";
		ofstream fexit;
		fexit.open("Coins.txt", ios::out);
		
		fexit << 0 << endl;

		fexit.close();
	}

	void Global::refreshCoins(int nCoins)
	{
		acumulatedCoins = acumulatedCoins + nCoins;
	}
	void Global::loadHighScore()
	{
		int i = 0;
		char line[20] = "";
		ifstream FileScoreEnt;
		FileScoreEnt.exceptions(ifstream::failbit | ifstream::badbit);
		try {
			FileScoreEnt.open("HighScores.txt", ios::in | ios::app);
			cout << "abriendo el archivo Scores" << endl;
			try {
				cout << "leyendo todo el archivo Scores" << endl;
				FileScoreEnt.getline(line, 20);
				while (!FileScoreEnt.eof()) {
					highScores[i] = stoi(line);
					cout << line << endl;
					FileScoreEnt.getline(line, 20);
					i++;
				}
				cout << line << endl;
			}
			catch (ifstream::failure& e) {
				if (FileScoreEnt.bad())
					cout << "hubo un error al leer el archivo Scores";
			}
			try { FileScoreEnt.close(); cout << "cerrando el archivo Scores" << endl; }
			catch (ifstream::failure& e) { if (FileScoreEnt.fail())cout << "Hubo un error al cerrar el archivo Scores"; }
		}
		catch (ifstream::failure& e) { if (FileScoreEnt.fail())cout << "Hubo un error al abrir el archivo Scores"; }
		catch (...) {
			cout << "error desconocido" << endl;
		}
	}

	void Global::saveHighScore()
	{
		char cadena[80] = "";
		ofstream fexit;
		fexit.open("HighScores.txt", ios::out);
		for (int i = 0; i < 10; i++)
		{
			fexit << highScores[i] << endl;
		}

		fexit.close();
	}
	void Global::resetHighScore()
	{
		char cadena[80] = "";
		ofstream fexit;
		fexit.open("HighScores.txt", ios::out);
		for (int i = 0; i < 10; i++)
		{
			fexit << 0 << endl;
		}

		fexit.close();
	}

	void Global::loadUnlocks()
	{
		int i = 0;
		char line[20] = "";
		ifstream FileUnlocksEnt;
		FileUnlocksEnt.exceptions(ifstream::failbit | ifstream::badbit);
		try {
			FileUnlocksEnt.open("Unlocks.txt", ios::in | ios::app);
			cout << "abriendo el archivo Unlocks" << endl;
			try {
				cout << "leyendo todo el archivo Unlocks" << endl;
				FileUnlocksEnt.getline(line, 20);
				while (!FileUnlocksEnt.eof()) {
					if (i < CHARACTERSAVALIEBLE)
					{
						unlockedCharacters[i] = stoi(line);
						i++;
					}
					else
					{
						unlockedBackgrounds[i- CHARACTERSAVALIEBLE] = stoi(line);
						i++;
					}
					cout << line << endl;
					FileUnlocksEnt.getline(line, 20);
				}
				cout << line << endl;
			}
			catch (ifstream::failure& e) {
				if (FileUnlocksEnt.bad())
					cout << "hubo un error al leer el archivo Unlocks";
			}
			try { FileUnlocksEnt.close(); cout << "cerrando el archivo Unlocks" << endl; }
			catch (ifstream::failure& e) { if (FileUnlocksEnt.fail())cout << "Hubo un error al cerrar el archivo Unlocks"; }
		}
		catch (ifstream::failure& e) { if (FileUnlocksEnt.fail())cout << "Hubo un error al abrir el archivo Unlocks"; }
		catch (...) {
			cout << "error desconocido" << endl;
		}
	}

	void Global::saveUnlocks()
	{
		char cadena[80] = "";
		ofstream fexit;
		fexit.open("Unlocks.txt", ios::out);
		for (int i = 0; i < (CHARACTERSAVALIEBLE+BACKGROUNDSAVALIEBLE); i++)
		{
			if (i < CHARACTERSAVALIEBLE)
			{
				fexit << unlockedCharacters[i] << endl;
			}
			else
			{
				fexit <<  unlockedBackgrounds[i - CHARACTERSAVALIEBLE] << endl;
			}
			
		}

		fexit.close();
	}


	
}

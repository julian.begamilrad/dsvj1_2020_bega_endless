#include "Game.h"
namespace ENDLESSJB
{
	Game::Game()
	{
		player.init();
		inGame = true;
		pause = false;
		seconds = STARTDEFAULTNUMBER;
		timer[SECONDS] = STARTDEFAULTNUMBER;
		timer[MINUTES] = STARTDEFAULTNUMBER;
		timer[HOURS] = STARTDEFAULTNUMBER;
		music = LoadMusicStream("res/GameMusic.mp3");
		musicOn = true;
		alreadyAttack = false;
		sGetCoin = LoadSound("res/GetCoin.ogg");
		sBreakBarrel = LoadSound("res/BarrelBroke.ogg");
		HUD = LoadTexture("res/Textures/HUD.png");
		for (int i = 0; i < 5; i++)
		{
			posBackground[i].x = 0;
			posBackground[i].y = -(GetScreenHeight() / 5);

			posBackground1[i].x = GetScreenWidth();
			posBackground1[i].y = -(GetScreenHeight() / 5);
		}
		posPause.x = 0;
		posPause.y = 0;

		posStonePath[0].x = 0;
		posStonePath[0].y = GetScreenHeight() - (GetScreenHeight() / 5);
		posStonePath[1].x = GetScreenWidth();
		posStonePath[1].y = GetScreenHeight() - (GetScreenHeight() / 5);

		posStonePath1[0].x = 0;
		posStonePath1[0].y = GetScreenHeight() - (GetScreenHeight() / 10);
		posStonePath1[1].x = GetScreenWidth();
		posStonePath1[1].y = GetScreenHeight() - (GetScreenHeight() / 10);
		pattern1.patternInit(1);
		pattern2.patternInit(2);
		points = STARTDEFAULTNUMBER;
		currentFrame = STARTDEFAULTNUMBER;
		framesCounter = STARTDEFAULTNUMBER;
		framesSpeed = 8;
		characterStatus = RUN;


		stonePath = LoadTexture("res/Textures/StonePath.png");
		stonePath1 = LoadTexture("res/Textures/StonePath1.png");
		pauseTexture = LoadTexture("res/Textures/Pause.png");

	}
	Game::~Game()
	{
		UnloadMusicStream(music);
		UnloadTexture(HUD);
		UnloadSound(sGetCoin);
		UnloadSound(sBreakBarrel);
		UnloadTexture(pauseTexture);
		UnloadTexture(stonePath);
		UnloadTexture(stonePath1);

	}
	void Game::gInit()
	{
		player.init();
		inGame = true;
		pause = false;
		seconds = STARTDEFAULTNUMBER;
		timer[SECONDS] = STARTDEFAULTNUMBER;
		timer[MINUTES] = STARTDEFAULTNUMBER;
		timer[HOURS] = STARTDEFAULTNUMBER;
		points = STARTDEFAULTNUMBER;
		musicOn = true;
		alreadyAttack = false;
		PlayMusicStream(music);
		pattern1.patternInit(1);
		pattern2.patternInit(2);
		for (int i = 0; i < 5; i++)
		{
			posBackground[i].x = 0;
			posBackground[i].y = -(GetScreenHeight() / 5);

			posBackground1[i].x = GetScreenWidth();
			posBackground1[i].y = -(GetScreenHeight() / 5);
		}

		posStonePath[0].x = 0;
		posStonePath[0].y = GetScreenHeight() - (GetScreenHeight() / 5);
		posStonePath[1].x = GetScreenWidth();
		posStonePath[1].y = GetScreenHeight() - (GetScreenHeight() / 5);

		posStonePath1[0].x = 0;
		posStonePath1[0].y = GetScreenHeight() - (GetScreenHeight() / 10);
		posStonePath1[1].x = GetScreenWidth();
		posStonePath1[1].y = GetScreenHeight() - (GetScreenHeight() / 10);
		currentFrame = STARTDEFAULTNUMBER;
		framesCounter = STARTDEFAULTNUMBER;
		framesSpeed = 8;
		characterStatus = RUN;
	}
	void Game::gInput()
	{
		player.input();
		if (IsKeyReleased(KEY_P))
		{
			pause = !pause;

			if (pause) PauseMusicStream(music);
			else if (musicOn) ResumeMusicStream(music);
		}
		if (IsKeyReleased(KEY_N))
		{
			if (musicOn)PauseMusicStream(music);
			else if (!pause)ResumeMusicStream(music);
			musicOn = !musicOn;
		}
		if (IsKeyReleased(KEY_A) && alreadyAttack == false)
		{
			attack(pattern1);
			attack(pattern2);
			characterStatus = ATTACK;
			currentFrame = 0;
			alreadyAttack = true;
		}
		if (pause == true)
		{
			if (IsKeyReleased(KEY_M))
			{
				Global::setGamestatus(MENU);
				gInit();
				pause = false;
			}

			if (IsKeyReleased(KEY_R))
			{
				gInit();
				pause = false;
			}
			
		}
		if (IsKeyReleased(KEY_SPACE))
		{
			if (player.getIsJumping() == false)
			{
				player.setIsJumping(true);
				
			}
		}

	}
	void Game::gUpdate()
	{
		if (player.getIsJumping() == true)
		{
			characterStatus = JUMP;
		}
		UpdateMusicStream(music);
		if (pause != true && player.getDeath() != true)
		{
			framesCounter++;
			if (framesCounter >= (60 / framesSpeed))
			{
				framesCounter = 0;
				currentFrame++;

				if (currentFrame > 9)
				{

					currentFrame = 0;
					characterStatus = RUN;
					alreadyAttack = false;
				}
			}
			gameTime();
			player.update();
			if (characterStatus != ATTACK)
			{
				parallax();
			}
			getColissions(pattern1);
			getColissions(pattern2);
			
		}
	}
	void Game::gDraw()
	{
		float baseScale = GetScreenHeight();
		float scale = baseScale / 1080;
		Vector2 pos;
		pos.x = 0; pos.y = 0;
		for (int i = 0; i < 5; i++)
		{
			DrawTextureEx(gameTextures.BackGrounds[i][Global::actualBackground], posBackground[i], 0, scale, WHITE);
			DrawTextureEx(gameTextures.BackGrounds[i][Global::actualBackground], posBackground1[i], 0, scale, WHITE);
		}
		DrawTextureEx(HUD, pos, 0, scale / 1.24, WHITE);

		DrawRectangle(0, GetScreenHeight() - (GetScreenHeight() / 5), GetScreenWidth(), GetScreenHeight() / 5, BROWN);
		DrawRectangle(0, GetScreenHeight() - (GetScreenHeight() / 10), GetScreenWidth(), GetScreenHeight() / 10, DARKBROWN);
		DrawTextureEx(stonePath, posStonePath[0], 0, scale / 1.6, WHITE);
		DrawTextureEx(stonePath, posStonePath[1], 0, scale / 1.6, WHITE);

		DrawTextureEx(stonePath1, posStonePath1[0], 0, scale / 1.6, WHITE);
		DrawTextureEx(stonePath1, posStonePath1[1], 0, scale / 1.6, WHITE);

		pattern1.patternDraw();
		pattern2.patternDraw();

		DrawText(TextFormat("TIME %1i", timer[MINUTES]), GetScreenWidth() / HALFDIVIDER - MeasureText("TIME 100 :", Global::TextSize) / HALFDIVIDER, GetScreenHeight() / 8 - Global::TextSize, Global::TextSize, WHITE);
		DrawText(TextFormat(": %1i", timer[SECONDS]), GetScreenWidth() / HALFDIVIDER + MeasureText("TIME 100 :", Global::TextSize) / 3, GetScreenHeight() / 8 - Global::TextSize, Global::TextSize, WHITE);
		player.draw();
		DrawTextureEx(gameTextures.playerSprites[Global::actualCharacter][characterStatus][currentFrame], player.getPosition(), 0, scale / 3, WHITE);
		DrawText(TextFormat("PTs %1i", points), GetScreenWidth() / 5.65 - MeasureText("PTs .....", Global::TextSize / HALFDIVIDER) , GetScreenHeight() / 7 - Global::TextSize, Global::TextSize/HALFDIVIDER, WHITE);
		if (pause == true)
		{
			DrawTextureEx(pauseTexture, posPause, 0, scale, WHITE);
		}
	}
	void Game::gameTime()
	{
		if (pause != true)
			seconds += GetFrameTime();
		if (seconds >= 1) {
			timer[SECONDS]++;
			points = points + 1;
			if (timer[SECONDS] >= 60) {
				timer[SECONDS] = 0;
				timer[MINUTES]++;
			}
			if (timer[MINUTES] >= 60) {
				timer[MINUTES] = 0;
				timer[HOURS]++;
			}
			seconds = 0;
		}
	}
	void Game::parallax()
	{
		pattern1.patternParallax();
		pattern2.patternParallax();
		for (int i = 0; i < 5; i++)
		{
			posBackground[i].x = posBackground[i].x - ((Global::parallaxMovment + (Global::parallaxMovment *i)) * GetFrameTime());
			posBackground1[i].x = posBackground1[i].x - ((Global::parallaxMovment + (Global::parallaxMovment * i)) * GetFrameTime());
			

			if (posBackground[i].x <= - GetScreenWidth()) 
			{
				posBackground[i].x = GetScreenWidth();
			}
			if (posBackground1[i].x <= -GetScreenWidth())
			{
				posBackground1[i].x = GetScreenWidth();
			}
		}
		for (int i = 0; i < 2; i++)
		{
			posStonePath[i].x = posStonePath[i].x - ((Global::parallaxMovment + (Global::parallaxMovment * 5)) * GetFrameTime());
			if (posStonePath[i].x <= -(GetScreenWidth() + GetScreenWidth() / 10))
			{
				posStonePath[i].x = GetScreenWidth();
			}
			posStonePath1[i].x = posStonePath1[i].x - ((Global::parallaxMovment + (Global::parallaxMovment * 5)) * GetFrameTime());
			if (posStonePath1[i].x <= -(GetScreenWidth() + GetScreenWidth() / 10))
			{
				posStonePath1[i].x = GetScreenWidth();
			}
		}
	}

	void Game::getColissions(SpawnPaterns& pattern)
	{

		for (int i = 0; i < 25; i++)
		{
			if (pattern.Pattern[player.getRoad()][i].type != NONE)
			{
				if (CheckCollisionRecs(player.getBody(), pattern.Pattern[player.getRoad()][i].Body))
				{
					switch (pattern.Pattern[player.getRoad()][i].type)
					{
					case COIN:
						PlaySound(sGetCoin);
						pattern.Pattern[player.getRoad()][i].type = NONE;
						player.setCoins(player.getCoins() + 1);
						points = points + 2;
						break;
					case DIAMOND:
						PlaySound(sGetCoin);
						pattern.Pattern[player.getRoad()][i].type = NONE;
						player.setCoins(player.getCoins() + 5);
						points = points + 5;
						break;
					case SKEY:
						PlaySound(sGetCoin);
						pattern.Pattern[player.getRoad()][i].type = NONE;
						player.setSKeys(player.getSKeys() + 1);
						break;
					case GKEY:
						PlaySound(sGetCoin);
						pattern.Pattern[player.getRoad()][i].type = NONE;
						player.setGKeys(player.getGKeys() + 1);
						break;
					case GCHEST:
						if (player.getGKeys() >= 1)
						{
						pattern.Pattern[player.getRoad()][i].type = GCHESTOPEN;
						player.setCoins(player.getCoins() + 10);
						player.setGKeys(player.getGKeys() - 1);
						points = points + 15;
						}
						break;
					case SCHEST:
						if (player.getSKeys() >= 1)
						{
							pattern.Pattern[player.getRoad()][i].type = SCHESTOPEN;
							player.setCoins(player.getCoins() + 5);
							player.setSKeys(player.getSKeys() - 1);
							points = points + 10;
						}
						break;
					case SPIKES:
						death();
						break;
					case BARREL:
						death();
						break;
					case POISON:
						death();
						break;
					default:
						break;
					}
				}
			}
		}

	}

	void Game::attack(SpawnPaterns& pattern)
	{
		for (int i = 0; i < 25; i++)
		{
			if (pattern.Pattern[player.getRoad()][i].type == BARREL)
			{
				if (CheckCollisionRecs(player.getAttackArea(), pattern.Pattern[player.getRoad()][i].Body))
				{
					PlaySound(sBreakBarrel);
					pattern.Pattern[player.getRoad()][i].type = BROKENBARREL;
				}
			}
		}
	}

	void Game::death()
	{
		player.setDeath(true);
		Global::setGamestatus(CREDITS);
		Global::refreshCoins(player.getCoins());
		Credits::setPoints(points);
		Credits::setCoins(player.getCoins());
		Credits::setTime(timer[HOURS], timer[MINUTES], timer[SECONDS]);
		gInit();
	}

}

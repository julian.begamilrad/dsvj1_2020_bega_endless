#ifndef GAME_H
#define GAME_H

#include <iostream>

#include "raylib.h"

#include "Global.h"
#include "Objetos/Player.h"
#include "SpawnPaterns.h"
#include "Textures.h"
#include "Credits.h"

using namespace std;

enum GamePatternNumber
{
	FIRST = 1, SECOND = 2
};
namespace ENDLESSJB
{
	class Game
	{
	public:

		Game();
		~Game();
		void gInit();
		void gInput();
		void gUpdate();
		void gDraw();
		Textures gameTextures;
	private:
		void gameTime();
		void parallax();

		void getColissions(SpawnPaterns &pattern);
		void attack(SpawnPaterns& pattern);
		void death();
		int points;
		bool alreadyAttack;
		Player player;
		float actualGround;
		bool inGame;
		bool pause;
		float seconds;
		int timer[TIMESECTION];
		Music music;
		Sound sGetCoin;
		Sound sBreakBarrel;
		bool musicOn;
		SpawnPaterns pattern1;
		SpawnPaterns pattern2;
		Texture2D HUD;
		Vector2 posBackground[5];
		Vector2 posBackground1[5];
		Vector2 posPause;

		Vector2 posStonePath[2];
		Vector2 posStonePath1[2];
		int currentFrame;
		int framesCounter;
		int framesSpeed;
		AnimationsPosibilities characterStatus;
		
		
		Texture2D pauseTexture;
		Texture2D stonePath;
		Texture2D stonePath1;
	};

}

#endif
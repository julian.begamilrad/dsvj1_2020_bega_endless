#ifndef GLOBAL_H
#define GLOBAL_H

#include<iostream>
#include<fstream> 
#include <sstream>

#include "raylib.h"

#include "Credits.h"
using namespace std;
namespace ENDLESSJB
{
	const int QUARTERDIVIDER = 4;
	const int HALFDIVIDER = 2;
	const int THIRDDIVIDER = 3;
	const int INVERTMULTIPLIER = -1;
	const int DOUBLE = 2;
	const int TRIPLE = 3;
	const int QUATRUPLE = 3;

	const int SHOWNSCORES = 10;

	const int STARTDEFAULTNUMBER = 0;
	
	const int TIMESECTION = 3;

	const int CHARACTERSAVALIEBLE = 3;
	const int BACKGROUNDSAVALIEBLE = 4;
	enum GameStage
	{
		MENU, GAME, CREDITS, CONFIGMENU, SHOP, HIGHSCORE, EXIT
	};
	enum TIME
	{
		SECONDS, MINUTES, HOURS
	};

	static class Global
	{
	public:
		Global();
		~Global();
		static int getGamestatus();
		static int getLastGamestatus();
		static void setSize();
		static void ChangeSize();

		static void setGamestatus(int ngstatus);
		static void setLastGamestatus(int nlgstatus);
		static void setActualCharacter(int ncharacter);
		static void setActualBackground(int nbackground);
		static int getActualCharacter();
		static int getActualBackground();

		static void loadCoins();
		static void saveCoins();
		static void resetCoins();
		static void refreshCoins(int nCoins);

		static void loadHighScore();
		static void saveHighScore();
		static void resetHighScore();

		static void loadUnlocks();
		static void saveUnlocks();

		static int gamestatus;
		static int lastGamestatus;
		static int actualCharacter;
		static int actualBackground;
		static int acumulatedCoins;

		static bool unlockedCharacters[CHARACTERSAVALIEBLE];
		static bool unlockedBackgrounds[BACKGROUNDSAVALIEBLE];


		static int highScores[SHOWNSCORES];

		static float parallaxMovment;

		static float TextSize;
		static float DISTANCEDEFAULT;
		static float PLAYERWIDTH;
		static float PLAYERHEIGHT;

	private:
	};

}
#endif
/*
MKDIR "$(TargetDir)res"
XCOPY "$(SolutionDir)res""$(TargetDir)res" / e / h / y / s
XCOPY "$(SolutionDir)Libreria/raylib/bin""$(TargetDir)" / e / h / y / s*/
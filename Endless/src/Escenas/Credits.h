#ifndef CREDITS_H
#define CREDITS_H

#include<iostream>
#include<fstream> 
#include <sstream>

#include "raylib.h"

#include "Objetos/Player.h"
using namespace std;
namespace ENDLESSJB
{
	class Credits
	{
	public:
		Credits();
		~Credits();
		void cInput();
		void cInit();
		void cDraw();

		static void setPoints(int nPoints);
		static void setTime(int hour, int min, int sec);
		static void setCoins(int nCoins);
		static int getCoins();
		static int getPoints();
	private:
		void refreshHighScore();
		bool alrreadyRefresh;
		bool showHighScores;
		static int points;
		static int hours;
		static int minutes;
		static int seconds;
		static int coins;
		Texture2D CreditsTexture;
	};

}
#endif
#include "Textures.h"
namespace ENDLESSJB
{
	Textures::Textures()
	{
		for (int i = 1; i < 5; i++)
		{
			sky[i-1] = LoadTexture(FormatText("res/Background/PNG/Forest%i/Layers/Sky.png", i));
			farBackground[i - 1] = LoadTexture(FormatText("res/Background/PNG/Forest%i/Layers/farBackground.png", i));
			ground[i - 1] = LoadTexture(FormatText("res/Background/PNG/Forest%i/Layers/Ground.png", i));
			middleBackground[i - 1] = LoadTexture(FormatText("res/Background/PNG/Forest%i/Layers/middleBackground.png", i));
			closeBackground[i - 1] = LoadTexture(FormatText("res/Background/PNG/Forest%i/Layers/Foreground.png", i));
		}
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 10; j++)
			{
				playerSprites[i][RUN][j] = LoadTexture(FormatText("res/PJs/PNG/%i/RUN%i.png", i, j));
				playerSprites[i][JUMP][j] = LoadTexture(FormatText("res/PJs/PNG/%i/JUMP%i.png", i, j));
				playerSprites[i][ATTACK][j] = LoadTexture(FormatText("res/PJs/PNG/%i/ATTACK%i.png", i, j));
				playerSprites[i][IDLE][j] = LoadTexture(FormatText("res/PJs/PNG/%i/IDLE%i.png", i, j));
			}
		}

		for (int i = 0; i < 4; i++)
		{
			BackGrounds[0][i] = sky[i];

			BackGrounds[1][i] = farBackground[i];
			BackGrounds[2][i] = middleBackground[i];
			BackGrounds[2][i] = closeBackground[i];
			BackGrounds[3][i] = ground[i];
		}

	}
	Textures::~Textures()
	{
		for (int i = 0; i < 3; i++)
		{
			UnloadTexture(sky[i]);
			UnloadTexture(farBackground[i]);
			UnloadTexture(ground[i]);
			UnloadTexture(middleBackground[i]);
			UnloadTexture(closeBackground[i]);
		}

	}
}

#include "Credits.h"
namespace ENDLESSJB
{
	int Credits::points = STARTDEFAULTNUMBER;
	int Credits::hours = STARTDEFAULTNUMBER;
	int Credits::minutes = STARTDEFAULTNUMBER;
	int Credits::seconds = STARTDEFAULTNUMBER;
	int Credits::coins = STARTDEFAULTNUMBER;
	Credits::Credits()
	{
		alrreadyRefresh = false;
		CreditsTexture = LoadTexture("res/Textures/Credits/Credits.png");
		Global::loadHighScore();
	}
	Credits::~Credits()
	{
		UnloadTexture(CreditsTexture);
	}
	void Credits::cInput()
	{
		if (IsKeyReleased(KEY_SPACE))
		{
				Global::setGamestatus(MENU);				
				refreshHighScore();
				alrreadyRefresh = false;
									
		}
		if (IsKeyReleased(KEY_ESCAPE))
		{
			Global::setGamestatus(EXIT);
			cInit();
		}
		if (IsKeyReleased(KEY_ENTER))
		{
			Global::setGamestatus(HIGHSCORE);
			refreshHighScore();
			alrreadyRefresh = false;
		}
	}
	void Credits::cInit()
	{
		points = STARTDEFAULTNUMBER;
		coins = STARTDEFAULTNUMBER;
		hours = STARTDEFAULTNUMBER;
		minutes = STARTDEFAULTNUMBER;
		seconds = STARTDEFAULTNUMBER;
		alrreadyRefresh = false;
	}
	void Credits::cDraw()
	{
		float baseScale = GetScreenHeight();
		float scale = baseScale / 1080;
		Vector2 pos;
		pos.x = 0; pos.y = 0;

		
			DrawTextureEx(CreditsTexture, pos, 0, scale/1.24, WHITE);

			DrawText(TextFormat("TIME %1i", minutes), GetScreenWidth() / HALFDIVIDER - MeasureText("TIME 100 :", Global::TextSize) / HALFDIVIDER, GetScreenHeight() / 4 , Global::TextSize, WHITE);
			DrawText(TextFormat(": %1i", seconds), GetScreenWidth() / HALFDIVIDER + MeasureText("TIME 100 :", Global::TextSize) / 3, GetScreenHeight() / 4 , Global::TextSize, WHITE);
			
			DrawText(TextFormat("PTs %1i", points), GetScreenWidth() / 5.65 - MeasureText("PTs .....", Global::TextSize / HALFDIVIDER), GetScreenHeight() / 4 , Global::TextSize, WHITE);
			DrawText(TextFormat("Coins %1i", coins), GetScreenWidth() - (GetScreenWidth() / 5.65) - MeasureText("PTs .....", Global::TextSize / HALFDIVIDER), GetScreenHeight() / 4, Global::TextSize, WHITE);
	}
	void Credits::setPoints(int nPoints)
	{
		points = nPoints;
	}
	void Credits::setTime(int hour, int min, int sec)
	{
		seconds = sec;
		minutes = min;
		hours = hour;
	}
	void Credits::setCoins(int nCoins)
	{
		coins = nCoins;
	}
	int Credits::getCoins()
	{
		return coins;
	}
	int Credits::getPoints()
	{
		return points;
	}

	void Credits::refreshHighScore()
	{
		if (alrreadyRefresh == false)
		{
			for (int i = 0; i < 10; i++)
			{
				if (Global::highScores[i] < points)
				{
					for (int j = 9; j >= i + 1; j--)
					{
						Global::highScores[j] = Global::highScores[j - 1];
					}
					Global::highScores[i] = points;
					i = 10;
				}
			}
			alrreadyRefresh = true;
		}
		cInit();
	}
}

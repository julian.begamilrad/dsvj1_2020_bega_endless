#ifndef SPAWNPATERNS_H
#define SPAWNPATERNS_H

#include "raylib.h"
#include "Global.h"
#include "Textures.h"
namespace ENDLESSJB
{
	enum PatternFills
	{
		NONE, COIN, SPIKES, SKEY, GKEY, SCHEST, GCHEST, BARREL, DIAMOND, GCHESTOPEN, SCHESTOPEN, POISON, BROKENBARREL
	};

	const int SPEED = GetScreenWidth() ;

	struct Objects
	{
		PatternFills type;
		Vector2 position;
		Rectangle Body;
		Vector2 DownPos;
		Vector2 UpPos;
		float speed;
		bool goUp;
	};
	class SpawnPaterns
	{
	public:
		SpawnPaterns();
		~SpawnPaterns();
		void patternInit(int myNumber);
		void patternDraw();
		void patternParallax();

		Objects Pattern[3][25];
		int PatternNumber;

		Textures gameTextures;

		bool allreadySetPattern;
		void setNewPattern();

		Texture2D coin;
		Texture2D poison;
		Texture2D spikes;
		Texture2D gchest;
		Texture2D schest;
		Texture2D gchestOpen;
		Texture2D schestOpen;
		Texture2D gkey;
		Texture2D skey;
		Texture2D barrel;
		Texture2D brokenBarrel;
		Texture2D diamond;
	private:
	};
}
#endif
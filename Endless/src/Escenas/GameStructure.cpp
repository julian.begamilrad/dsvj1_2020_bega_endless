#include "GameStructure.h"
namespace ENDLESSJB
{
	GameStructure::GameStructure()
	{
		InitWindow(screenWidth, screenHeight, "ENDLESS Knight v1.1");
		InitAudioDevice();
		game = new Game();
		menu = new Menu();
		credits = new Credits();
		shop = new Shop();
		highscores = new HighScores();
		Global::setGamestatus(MENU); 
		Global::setLastGamestatus(MENU);
		firstTime = true;
		inGame = false;
		Global::setSize();
		Global::loadCoins();
		Global::loadHighScore();
	}
	GameStructure::~GameStructure()
	{
		gsDeInit();
		Global::saveCoins();
		Global::saveHighScore();
	}
	void GameStructure::gameLoop()
	{
		gsInit();
		while (inGame && !WindowShouldClose())
		{
			BeginDrawing();
			ClearBackground(DARKBROWN);
			gsInput();
			gsUpdate();
			gsDraw();
		}
	}
	void GameStructure::gsInit()
	{
		SetTargetFPS(60);
		menu->mInit();
		game->gInit();
		credits->cInit();
		shop->Init();
		Global::setGamestatus(MENU);
		Global::setLastGamestatus(MENU);
		inGame = true;
	}
	void GameStructure::gsInput()
	{
		switch (Global::getGamestatus())
		{
		case MENU:
			menu->mInput();
			break;
		case GAME:
			game->gInput();
			break;
		case CREDITS:
			credits->cInput();
			break;
		case SHOP:
			shop->Input();
			break;
		case HIGHSCORE:
			highscores->Input();
			break;
		case EXIT:
			inGame = false;
			break;
		}
		if (IsKeyReleased(KEY_T))
		{
			Global::ChangeSize();
			gsInit();
		}
	}
	void GameStructure::gsUpdate()
	{
		switch (Global::getGamestatus())
		{
		case MENU:
			Global::setLastGamestatus(Global::getGamestatus());
			menu->mUpdate();
			break;
		case GAME:
			Global::setLastGamestatus(Global::getGamestatus());
			game->gUpdate();
			break;
		case CREDITS:
			Global::setLastGamestatus(Global::getGamestatus());
			break;	
		case SHOP:
			Global::setLastGamestatus(Global::getGamestatus());
			shop->Update();
			break;
		case HIGHSCORE:
			Global::setLastGamestatus(Global::getGamestatus());
			highscores->Update();
			break;
		case EXIT:
			inGame = false;
			break;
		}
	}
	void GameStructure::gsDraw()
	{
		switch (Global::getGamestatus())
		{
		case MENU:
			menu->mDraw();
			break;
		case GAME:
			game->gDraw();
			break;
		case CREDITS:
			credits->cDraw();
			break;
		case SHOP:
			shop->sDraw();
			break;
		case HIGHSCORE:
			highscores->Draw();
			break;
		case EXIT:
			inGame = false;
			break;
		}
		EndDrawing();
	}
	void GameStructure::gsDeInit()
	{
		CloseAudioDevice();
		CloseWindow();
	}
}

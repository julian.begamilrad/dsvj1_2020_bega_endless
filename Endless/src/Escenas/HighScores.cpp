#include "HighScores.h"
namespace ENDLESSJB
{
	HighScores::HighScores()
	{
		scoreBase = LoadTexture("res/Textures/Credits/HighScore.png");
		currentFrame = STARTDEFAULTNUMBER;
		framesCounter = STARTDEFAULTNUMBER;
		framesSpeed = 8;
	}
	HighScores::~HighScores()
	{
		UnloadTexture(scoreBase);
	}
	void HighScores::Init()
	{
	}
	void HighScores::Input()
	{
		if (IsKeyReleased(KEY_ENTER))
		{
			Global::setGamestatus(MENU);
		}
		if (IsKeyReleased(KEY_ESCAPE))
		{
			Global::setGamestatus(EXIT);
		}
		if (IsKeyReleased(KEY_SPACE))
		{
			Global::setGamestatus(CREDITS);

		}
	}
	void HighScores::Update()
	{
	}
	void HighScores::Draw()
	{
		float baseScale = GetScreenHeight();
		float scale = baseScale / 1080;
		Vector2 pos;
		pos.x = 0; pos.y = 0;

		DrawTextureEx(scoreBase, pos, 0, scale / 1.24, WHITE);
		for (int i = 9; i >= 0; i--)
		{
			DrawText(TextFormat("- %1i", i + 1), GetScreenWidth() - (GetScreenWidth()/3), (GetScreenHeight() / 5) + ((Global::TextSize * 0.75) * i), Global::TextSize/4, WHITE);
			DrawText(TextFormat("Points: %1i", Global::highScores[i]), GetScreenWidth() - (GetScreenWidth() / 4),(GetScreenHeight() / 5) + ((Global::TextSize * 0.75) * i), Global::TextSize/4, WHITE);
		}
		DrawText(TextFormat("- %1i", 1), GetScreenWidth() - (GetScreenWidth() / 3), (GetScreenHeight() / 5) , Global::TextSize / 4, YELLOW);
		DrawText(TextFormat("Points: %1i", Global::highScores[0]), GetScreenWidth() - (GetScreenWidth() / 4), (GetScreenHeight() / 5), Global::TextSize / 4, YELLOW);
		DrawText(TextFormat("- %1i", 2), GetScreenWidth() - (GetScreenWidth() / 3), (GetScreenHeight() / 5) + (Global::TextSize * 0.75), Global::TextSize / 4, LIGHTGRAY);
		DrawText(TextFormat("Points: %1i", Global::highScores[1]), GetScreenWidth() - (GetScreenWidth() / 4), (GetScreenHeight() / 5) + (Global::TextSize * 0.75) , Global::TextSize / 4, LIGHTGRAY);

	}
	
}

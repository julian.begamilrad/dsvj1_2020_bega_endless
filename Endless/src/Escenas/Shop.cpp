#include "Shop.h"
namespace ENDLESSJB
{
	Shop::Shop()
	{
		shopBase = LoadTexture("res/Textures/Shop/ShopBase.png");
		currentFrame = STARTDEFAULTNUMBER;
		framesCounter = STARTDEFAULTNUMBER;
		framesSpeed = 8;
		Global::loadUnlocks();

		for (int i = 0; i < 3; i++)
		{
			if (Global::unlockedCharacters[i] == true) charactersPrice[i] = 0;
			if (Global::unlockedCharacters[i] == false) charactersPrice[i] = 300;
		}
		for (int i = 0; i < 4; i++)
		{
			if (Global::unlockedBackgrounds[i] == true) backgroundsPrice[i] = 0;
			if (Global::unlockedBackgrounds[i] == false) backgroundsPrice[i] = 200;

		}
		actualPrice = STARTDEFAULTNUMBER;
	}
	Shop::~Shop()
	{
		UnloadTexture(shopBase);
		Global::saveUnlocks();
	}
	void Shop::Init()
	{
		currentFrame = STARTDEFAULTNUMBER;
		framesCounter = STARTDEFAULTNUMBER;
		framesSpeed = 8;
		actualPrice = STARTDEFAULTNUMBER;
		for (int i = 0; i < 3; i++)
		{
			if (Global::unlockedCharacters[i] == true) charactersPrice[i] = 0;
			if (Global::unlockedCharacters[i] == false) charactersPrice[i] = 300;
		}
		for (int i = 0; i < 4; i++)
		{
			if (Global::unlockedBackgrounds[i] == true) backgroundsPrice[i] = 0;
			if (Global::unlockedBackgrounds[i] == false) backgroundsPrice[i] = 200;

		}
	}
	void Shop::Input()
	{
		if (IsKeyReleased(KEY_ENTER))
		{
			if (Global::unlockedCharacters[Global::getActualCharacter()] == true && Global::unlockedBackgrounds [Global::getActualBackground()] == true)
			{
				Global::setGamestatus(MENU);
				Global::saveUnlocks();
			}
			else
			{
				if (Global::acumulatedCoins >= actualPrice)
				{
					Global::acumulatedCoins = Global::acumulatedCoins - actualPrice;
					Global::unlockedCharacters[Global::getActualCharacter()] = true;
					Global::unlockedBackgrounds[Global::getActualBackground()] = true;
				}
			}
		}
		if (IsKeyReleased(KEY_ESCAPE))
		{
			Global::setGamestatus(EXIT);
		}
		if (IsKeyReleased(KEY_UP))
		{
			if (Global::getActualCharacter() >= 2)
			{
				Global::setActualCharacter(0);
			}
			else
			{
				Global::setActualCharacter(Global::getActualCharacter() + 1);
			}
		}
		if (IsKeyReleased(KEY_DOWN))
		{
			if (Global::getActualCharacter() <= 0)
			{
				Global::setActualCharacter(2);
			}
			else
			{
				Global::setActualCharacter(Global::getActualCharacter() - 1);
			}
		}
		if (IsKeyReleased(KEY_RIGHT))
		{
			if (Global::getActualBackground() >= 3)
			{
				Global::setActualBackground(0);
			}
			else
			{
				Global::setActualBackground(Global::getActualBackground()+1);
			}
		}
		if (IsKeyReleased(KEY_LEFT))
		{
			if (Global::getActualBackground() <= 0)
			{
				Global::setActualBackground(3);
			}
			else
			{
				Global::setActualBackground(Global::getActualBackground() - 1);
			}
		}
	}
	void Shop::Update()
	{
		framesCounter++;
		if (framesCounter >= (60 / framesSpeed))
		{
			framesCounter = 0;
			currentFrame++;

			if (currentFrame > 9) currentFrame = 0;
		}
		for (int i = 0; i < 3; i++)
		{
			if (Global::unlockedCharacters[i] == true) charactersPrice[i] = 0;
			if (Global::unlockedCharacters[i] == false) charactersPrice[i] = 300;
		}
		for (int i = 0; i < 4; i++)
		{
			if (Global::unlockedBackgrounds[i] == true) backgroundsPrice[i] = 0;
			if (Global::unlockedBackgrounds[i] == false) backgroundsPrice[i] = 200;

		}
		actualPrice = backgroundsPrice[Global::actualBackground] + charactersPrice[Global::actualCharacter];
	}
	void Shop::sDraw()
	{
		float baseScale = GetScreenHeight();
		float scale = baseScale / 1080;
		Vector2 pos;
		pos.x = 0; pos.y = 0;

		for (int i = 0; i < 5; i++)
		{
			DrawTextureEx(shopTextures.BackGrounds[i][Global::actualBackground], pos, 0, scale, WHITE);
		}

		Vector2 posPlayer;
		posPlayer.x = GetScreenWidth() / 3; posPlayer.y = GetScreenHeight() / 3;
		float PJScale = scale / 1.3;


		DrawTextureEx(shopTextures.playerSprites[Global::actualCharacter][IDLE][currentFrame], posPlayer, 0, PJScale, WHITE);
		posPlayer.x = GetScreenWidth() / 12;
		DrawTextureEx(shopTextures.playerSprites[Global::actualCharacter][RUN][currentFrame], posPlayer, 0, PJScale, WHITE);
		posPlayer.x = -GetScreenWidth() / 5;
		DrawTextureEx(shopTextures.playerSprites[Global::actualCharacter][ATTACK][currentFrame], posPlayer, 0, PJScale, WHITE);

		DrawTextureEx(shopBase, pos, 0, scale / 1.24, WHITE);
		DrawText(TextFormat(" %1i", Global::acumulatedCoins), GetScreenWidth() - (GetScreenWidth() / 7) - MeasureText(":", Global::TextSize / HALFDIVIDER) / HALFDIVIDER, GetScreenHeight() / 11.3 , Global::TextSize / HALFDIVIDER, WHITE);
		if (actualPrice != 0)
		{
		DrawText(TextFormat(" %1i", actualPrice), GetScreenWidth() - (GetScreenWidth() / 4) , GetScreenHeight() -( GetScreenHeight() / 11.3), Global::TextSize / HALFDIVIDER, WHITE);
		}
		else
		{
			DrawText("OWNED", GetScreenWidth() - (GetScreenWidth() / 4), GetScreenHeight() - (GetScreenHeight() / 11.3), Global::TextSize / HALFDIVIDER, WHITE);
		}
	}
}
